package hungrybot

import org.jsoup.Jsoup
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.File

class LetsPlayParserKtTest {

    @Test
    fun parseLetsPlay() {
        val input = File("src/test/resources/letsplay.html")

        val doc =
            Jsoup.parse(
                input,
                "UTF-8",
                "https://www.amica.fi/en/restaurants/ravintolat-kaupungeittain/espoo/lets-play/"
            )

        val res = parseLetsPlay(doc)
        assertTrue(res.contains("Carrot-lentil"))
    }
}