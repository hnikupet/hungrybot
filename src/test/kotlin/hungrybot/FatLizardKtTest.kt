package hungrybot

import org.jsoup.Jsoup
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.File
import java.time.LocalDate

class FatLizardKtTest {

    @Test
    fun shouldParseBasicCaseCorretly() {
        val input = File("src/test/resources/fatlizard.html")

        val doc =
            Jsoup.parse(
                input,
                "UTF-8",
                "https://ravintolafatlizard.fi/lunch/?lang=en"
            )

        val date = LocalDate.of(2019, 7, 5)
        val res = parseFatLizard(doc, date)
        println("res = ${res}")
        assertTrue(res.contains("Roasted paprika soup"))
    }

    @Test
    fun shouldParseBasicCaseCorrectly2() {
        val input = File("src/test/resources/fatlizard2.html")

        val doc =
            Jsoup.parse(
                input,
                "UTF-8",
                "https://ravintolafatlizard.fi/lunch/?lang=en"
            )

        val date = LocalDate.of(2019, 7, 16)
        val res = parseFatLizard(doc, date)
        println("res = ${res}")
        assertTrue(res.contains("Sweet potato soup"))
    }

    @Test
    fun shouldNotContainFutureDays() {
        val input = File("src/test/resources/fatlizard3.html")

        val doc =
            Jsoup.parse(
                input,
                "UTF-8",
                "https://ravintolafatlizard.fi/lunch/?lang=en"
            )

        val date = LocalDate.of(2019, 8, 26)
        val res = parseFatLizard(doc, date)
        println("res = ${res}")
        assertTrue(res.contains("Tom Yam rice noodle soup"))
        assertFalse(res.contains("Gazpacho"))
    }


}