package hungrybot

import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.jsoup.Jsoup
import org.junit.Test
import java.io.File
import java.time.LocalDate

class FactoryParserKtTest {

    @Test
    fun shouldParseFactoryCorrectly() {

        val input = File("src/test/resources/factory.html")

        val doc =
            Jsoup.parse(input, "UTF-8", "https://ravintolafactory.com/lounasravintolat/ravintolat/espoo-keilaranta/")

        val date = LocalDate.of(2019, 6, 3)
        val res = parseFactory(doc, date)
        assertTrue(res.contains("Italian tomato soup"))
    }

    @Test
    fun shouldParseFactoryCorrectlyAlsoThis() {

        val input = File("src/test/resources/factory2.html")

        val doc =
            Jsoup.parse(input, "UTF-8", "https://ravintolafactory.com/lounasravintolat/ravintolat/espoo-keilaranta/")

        val date = LocalDate.of(2019, 6, 19)
        val res = parseFactory(doc, date)
        assertTrue(res.contains("Sweet potato soup"))
    }


    @Test
    fun handleSteakFriday() {

        val input = File("src/test/resources/factory.html")

        val doc =
            Jsoup.parse(input, "UTF-8", "https://ravintolafactory.com/lounasravintolat/ravintolat/espoo-keilaranta/")

        val date = LocalDate.of(2019, 6, 7)
        val res = parseFactory(doc, date)
        // println("res = ${res}")
        assertTrue(res.contains("Smooth asparagus soup"))
    }

    @Test
    fun shouldSkipPorridgeOnMenu() {

        val input = File("src/test/resources/factory3.html")

        val doc =
            Jsoup.parse(input, "UTF-8", "https://ravintolafactory.com/lounasravintolat/ravintolat/espoo-keilaranta/")

        val date = LocalDate.of(2019, 7, 10)
        val res = parseFactory(doc, date)
        println("res = ${res}")
        assertTrue(res.contains("Creamy fish soup"))
        assertFalse(res.contains("Semolina porridge"))
    }

}
