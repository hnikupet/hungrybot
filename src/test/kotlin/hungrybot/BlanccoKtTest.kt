package hungrybot

import org.jsoup.Jsoup
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.File
import java.time.LocalDate

class BlanccoKtTest {

    @Test
    fun shouldParseBasicCaseCorretly() {
        val input = File("src/test/resources/blancco.html")

        val doc =
            Jsoup.parse(
                input,
                "UTF-8",
                "https://www.ravintolablancco.com/lounas-ravintolat/keilaranta/"
            )

        val date = LocalDate.of(2019, 6, 4)
        val res = parseBlancco(doc, date)
        // println("res = ${res}")
        assertTrue(res.contains("Sausage Soup"))
    }

    @Test
    fun shouldParseAnotherBasicCaseCorrectly() {
        val input = File("src/test/resources/blancco2.html")

        val doc =
            Jsoup.parse(
                input,
                "UTF-8",
                "https://www.ravintolablancco.com/lounas-ravintolat/keilaranta/"
            )

        val date = LocalDate.of(2019, 6, 26)
        val res = parseBlancco(doc, date)
        //println("res = ${res}")

        // should only display wednesday menu:
        assertTrue(res.contains("Zucchini Soup"))
        assertFalse(res.contains("Vietnamese Chicken Soup"))
        assertFalse(res.contains("Pea soup"))
    }
}