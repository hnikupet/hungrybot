package hungrybot

import org.jsoup.Jsoup
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import java.io.File
import java.time.LocalDate

class LucyParserKtTest {

    @Test
    fun parseLucy() {
        val input = File("src/test/resources/lunch_lucy.html")

        val doc =
            Jsoup.parse(
                input,
                "UTF-8",
                "https://en.lucyinthesky.fi/lucy-lunch"
            )
        val date = LocalDate.of(2020, 1, 28)
        val res = parseLucy(doc, date)
        assertTrue(res.contains("Miso"))
    }

    @Test
    fun parseLucy2() {
        val input = File("src/test/resources/lunch_lucy2.html")

        val doc =
            Jsoup.parse(
                input,
                "UTF-8",
                "https://en.lucyinthesky.fi/lucy-lunch"
            )
        val date = LocalDate.of(2020, 1, 29)
        val res = parseLucy(doc, date)
        assertTrue(res.contains("Wild mushroom and miso soup with blooming wakame"))
        assertFalse(res.contains("Matcha latte"))
        assertFalse(res.contains("Very Wild mushroom and miso soup with blooming wakame"))
    }
}