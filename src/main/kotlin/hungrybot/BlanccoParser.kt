package hungrybot

import org.jsoup.nodes.Document
import java.time.LocalDate
import java.time.format.DateTimeFormatter

fun parseBlancco(doc: Document, date: LocalDate): String {

    val formatter = DateTimeFormatter.ofPattern("E")
    val today = formatter.format(date).toUpperCase()
    val tomorrow = formatter.format(date.plusDays(1)).toUpperCase()

    val menuElements = doc.select("div.page-content");

    var isDayFound = false
    var food = "";
    for (menu in menuElements) {
        for (day in menu.children()) {
            if (isDayFound) {
                if ((day.children().isNotEmpty() && day.child(0).tagName() == "strong") || day.text().startsWith(tomorrow))
                    break

                food = food + "\n" + day.text()
            }
            if (day.text().startsWith(today)) {
                isDayFound = true
            }
        }
        if (isDayFound) {
            break
        }
    }

    println("Parsed Blancco menu....")

    if ("" == food) {
        food = "\n:man-shrugging: :woman-shrugging:\n"
    }


    return "\n*Blancco*\n" + food
}