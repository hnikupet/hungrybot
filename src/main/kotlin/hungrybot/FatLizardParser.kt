package hungrybot

import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import org.jsoup.select.Elements
import java.time.LocalDate
import java.time.format.DateTimeFormatter

val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("E")
fun parseFatLizard(doc: Document, date: LocalDate): String {

    val menuElements = doc.select("div.wpb_wrapper")

    var food = parseMenuElements(menuElements, date)
    println("Parsed Fat Lizard menu....")

    if ("" == food) {
        food = "\n:man-shrugging: :woman-shrugging: ??\n"
    }

    return "\n*FatLizard*\n" + food
}

fun parseMenuElements(menuElements: Elements, date: LocalDate): String {
    var food = ""
    for (menu in menuElements) {
        food = parseMenu(menu, date)
        if ("" != food) {
            break
        }

    }
    return food
}

fun parseMenu(menu: Element, date: LocalDate): String {
    var isDayFound = false
    var food = ""
    val today = formatter.format(date)
    val tomorrow = formatter.format(date.plusDays(1)).toUpperCase()

    for (day in menu.children()) {
        val wrapperGrandChildren = day.select("div.wpb_wrapper")
        if (!wrapperGrandChildren.isEmpty())
            return parseMenuElements(wrapperGrandChildren, date)

        val dataBlock = day.select("div[data-block]")
        if (!dataBlock.isEmpty())
            return parseMenuElements(dataBlock, date)

        if (isDayFound) {
            if ((day.children().isNotEmpty() && day.child(0).tagName() == "strong") || day.text().toUpperCase().startsWith(tomorrow)) {
                break
            }
            food = food + "\n" + day.text()
        }
        if (day.text().startsWith(today)) {
            isDayFound = true
        }
    }
    return food
}
