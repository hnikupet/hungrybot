package hungrybot

import org.jsoup.nodes.Document
import java.time.LocalDate
import java.time.format.DateTimeFormatter

fun parseLucy(doc: Document, date: LocalDate): String {

    val menuElements = doc.select("div#comp-k586yeyr")
    var food = ""
    val formatter = DateTimeFormatter.ofPattern("E")
    val today = formatter.format(date).toUpperCase()
    val tomorrow = formatter.format(date.plusDays(1)).toUpperCase()
    var isNext = false

    for (menu in menuElements.select("p")) {

        if (menu.text().startsWith(tomorrow)) {
            // already tomorrow's menu
            break
        }

        if (isNext)
            food += menu.text() + "\n"

        if (menu.text().startsWith(today)) {
            isNext = true
        }
    }


    if ("" == food) {
        food = "\n:man-shrugging: :woman-shrugging:\n"
    }

    println("Parsed Lucy's menu....")

    return "\n*Lucy in the Sky*\n$food"
}