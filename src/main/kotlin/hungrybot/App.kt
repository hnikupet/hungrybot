package hungrybot

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Klaxon
import org.jsoup.Jsoup
import java.io.StringReader
import java.net.URL
import java.time.LocalDate
import java.time.format.DateTimeFormatter

fun main(args: Array<String>) {

    val slackUrl = getSlackUrlFromArgs(args)
    println("Starting...")

    val factoryMenu = fetchFactory()
    val blanccoMenu = fetchBlancco()
    val letsPlayMenu = fetchLetsPlay()
    val fatLizardMenu = fetchFatLizard()
    val lucyMenu = fetchLucy()

    val menus =
        "Today's menu is...:stuck_out_tongue: :stuck_out_tongue:\n" + pancakeHighlighter(factoryMenu) + "\n" + pancakeHighlighter(
            blanccoMenu
        ) + "\n" + pancakeHighlighter(letsPlayMenu) + "\n" + pancakeHighlighter(fatLizardMenu) + "\n" + pancakeHighlighter(lucyMenu)

    print(menus)
    postToSlack(menus, slackUrl)
}

fun getSlackUrlFromArgs(args: Array<String>): String {
    return if (args.isEmpty()) {
        println("Please provide the Slack API url as a command-line argument. Now doing a dry run")
        ""
    } else {
        args[0]
    }
}

fun postToSlack(menus: String, slackHookUrl: String) {

    if ("" != slackHookUrl) {
        khttp.post(
            url = slackHookUrl,
            json = mapOf("username" to "Happy HungryBot", "icon_emoji" to ":stuck_out_tongue:", "text" to menus)
        )
        print("Sent menus to Slack!")
    } else {
        println("\nJust a dry run, nothing sent to slack")
    }
}

private fun pancakeHighlighter(menuOfDay: String): String {

    return menuOfDay
        .replace("pancakes", ":pancakes:")
        .replace("pizza", ":pizza:")
        .replace("strawberry", ":strawberry:")
        .replace("tomato", ":tomato:")
        .replace(" rice ", " :rice: ")
        .replace("carrot", ":carrot:")
        .replace("chicken kiev", "*chicken kiev*")
        .replace("chicken", ":chicken:")
}

private fun fetchFatLizard(): String {
    val doc = Jsoup.connect("https://ravintolafatlizard.fi/lunch/?lang=en").get()
    val date = LocalDate.now()
    return parseFatLizard(doc, date)
}

private fun fetchFactory(): String {
    val doc = Jsoup.connect("https://ravintolafactory.com/lounasravintolat/ravintolat/espoo-keilaranta/").get()
    val date = LocalDate.now()
    return parseFactory(doc, date)
}

private fun fetchBlancco(): String {
    val doc = Jsoup.connect("https://www.ravintolablancco.com/lounas-ravintolat/keilaranta/").get()
    return parseBlancco(doc, LocalDate.now())
}

private fun fetchLucy(): String {
    val doc = Jsoup.connect("https://en.lucyinthesky.fi/lucy-lunch").get()
    return parseLucy(doc, LocalDate.now())
}

private fun fetchLetsPlay(): String {

    val date = LocalDate.now()
    val formatter = DateTimeFormatter.ofPattern("Y-M-d")

    //println("date: " + formatter.format(date));
    val url =
        "https://www.amica.fi/api/restaurant/menu/day?date=" + formatter.format(date) + "&language=en&restaurantPageId=46009"
    //println(url);
    val apiResponse: String = URL(url).readText()

    val klaxon = Klaxon()
    val parsed = klaxon.parseJsonObject(StringReader(apiResponse))
    val menu: JsonObject? = parsed.obj("LunchMenu")

    return if (menu != null) {
        val html = menu.string("Html")
        val doc = Jsoup.parse(html)
        parseLetsPlay(doc)
    } else {
        "Sorry! Failed to parse Let's Play! :confused:"
    }
}

