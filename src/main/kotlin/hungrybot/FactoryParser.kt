package hungrybot

import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.time.LocalDate
import java.time.format.DateTimeFormatter

fun parseFactory(doc: Document, dateTime: LocalDate): String {

    val formatter = DateTimeFormatter.ofPattern("E")
    val today = formatter.format(dateTime)

    val menuElements = doc.select("div.col-md-12.col-xs-12.nopadding");
    var isNext = false;
    var food = ""
    for (menu in menuElements) {
        for (day in menu.children()) {
            if (isNext) {
                // check if the element is steak or ice cream image, skip those.
                val isImg = isOrContainsImgElement(day)
                val isPorridge = isPorridge(day);
                if (!isImg && !isPorridge) {
                    food = day.html().replace("<br> ", "\n").replace("<br>", "\n")
                    break;
                }
            }
            if (day.text().startsWith(today)) {
                isNext = true
            }
        }
    }
    println("Parsed Factory menu....")

    if ("" == food) {
        food = "\n:man-shrugging: :woman-shrugging:\n"
    }

    return "\n*Factory*\n$food"
}

fun isPorridge(ele: Element): Boolean {
    return ele.text().toLowerCase().contains("porridge");
}

fun isOrContainsImgElement(ele: Element): Boolean {
    return if (ele.children().isEmpty())
        ele.tagName() == "img"
    else
        ele.children()[0].tagName() == "img";
}
