package hungrybot

import org.jsoup.nodes.Document

fun parseLetsPlay(doc: Document): String {

    val menuElements = doc.select("body");
   // println("letsplay menu size: ${menuElements.size}")
    var food = "";
    for (menu in menuElements) {
       // println(menu)

        for (day in menu.children()) {
            //println(day)
            // find empty element, which changes food element.
            if ("" == (day.text().trim()))
                food += "\n"
            else
                food = food + "\n" + day.text()

        }
    }

    if ("" == food) {
        food = "\n:man-shrugging: :woman-shrugging:\n"
    }

    println("Parsed Let's Play menu....")

    return "\n*Let's Play*\n$food"
}