# Otaniemi Lunch Menu Slack bot #

Fetched lunch menus around lunch places in Otaniemi and Keilaniemi region and posts them to Slack with a given Slack Hook.

The app was originally written to try out Kotlin and hence can probably benefit greatly from improvements.

## Building ##

To build, run gradle command: ``gradle build``

## Usage ##

To run, type: ``java -jar hungrybot-1.0-SNAPSHOT.jar <slack hook url>``

Ideally, copy the fat jar created in the ``build/libs`` and set it as a cron job to your server. A good time to run would be at 11.15 Finnish time when most restaurants have already published their daily menus.

## Development ##
Feel free to add new features and restaurants and otherwise improve the architecture.